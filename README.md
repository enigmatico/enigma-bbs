![Project Banner](/docs/res/banner.jpg)

# enigma-bbs

This is a personal project for a simple text-only bulletin board engine. It's currently on the works and it still doesn't work properly,
plus it lacks of some basic features. For now, it doesn't even have basic features such as moderation or a control panel, so everything
has to be done manually by hand in the data base.

## Requirements

- A web server (Configured with Apache, nginx...)
- PHP 7+ (Something that has the mysqli extension)
- MySQL (Tested with version 8 but should work with earlier versions)
- FTP access to your server, or some way to transfer files to it
- A web browser. Any web browser will do.
- Patience (It's a quite primitive piece of software)

If you are on Windows and you want a quick test you can use something like [XAMPP](https://www.apachefriends.org/index.html). In Linux,
you can check out this guide from HowtoForge [on how to install Nginx with PHP and MySQL](https://www.howtoforge.com/tutorial/how-to-install-nginx-with-php-and-mysql-lemp-on-ubuntu-20-04/).

## Installation

1. Open data/config/.config.php with your text editor.
2. Change the value of each option accordingly.
3. Open data/config/SQL/.trusted.php
4. In trusted_locations, add your machine IP address to the list (if you are installing from a remote machine)
5. Upload the whole content of the web folder to your web server (wherever you are installing it)
6. From your web browser, load the install.php script

## Initial configuration

__To make your site functional, you'll have to manually configure your site by editing the database. I recommend having phpmyadmin for this.__

1. Add a new row to the sm_config table in your database with the name of your site, the description, some MOTD in the news, and set
the default_stile column to solarized.css (The only available style as of now).
2. Add some categories to the sm_board_category table. You only need to insert the name. Take note of the IDs.
3. Now create some boards by inserting some rows to the sm_boards table. The PREFIX column is just a prefix. Set the CATEGORY to the ID of the
corresponding category in sm_board_category.

In order to set up the headers for each board, you also need to create a new folder in static/banners. The name of the folder will be the ID
of the board. Inside of each folder you can add as many images as you want (recommended size 1024x200 for eahc), the script will choose a random one.

__That's it!! You're finally ready.__


![Project Overview](/docs/res/overview.png)

## Browser compatibility

By default the site will render properly in pretty much any modern web browser. However, I tested it on Netscape 7.0 from back in 2002 and it is still usable. That means that pretty much any browser from the early-mid 2000s to our current times should be able to somehow render it in a way it is usable even if it doesn't look as it should...

![Project Overview](/docs/res/enigmabbs.png)